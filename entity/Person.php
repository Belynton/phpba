<?php

class Person
{

    public function __construct()
    {
        $this->generateRandomPersonData();
    }

    const FIRST_NAMES = ["Александр", "Владимир", "Виктор", "Антон", "Женя", "Петр", "Иван", "Михаил", "Никита", "Денис"];
    const SURNAMES = ["Петров", "Иванов", "Сидоров", "Казаков", "Игнатьев", "Козлов", "Волов", "Симонов", "Давыдов", "Ларин"];
    const SECOND_NAMES = ["Александрович", "Владимирович", "Викторович", "Антонович", "Евгеньевич", "Петрович", "Иванович", "Михаилович", "Никитич", "Денисович"];
    const REGISTRATION_TYPES = ["Постоянная", "Временная", "Без регистрации"];

    private string $firstName;
    private string $surname;
    private string $secondName;
    private string $snils;
    private string $registrationType;
    private string $birthDate;
    private string $registrationDate;
    private ?string $moveOutDate;

    private function generateRandomPersonData(): void
    {
        $this->firstName = self::FIRST_NAMES[array_rand(self::FIRST_NAMES)];
        $this->surname = self::SURNAMES[array_rand(self::SURNAMES)];
        $this->secondName = self::SECOND_NAMES[array_rand(self::SECOND_NAMES)];
        $this->snils = $this->generateSnils();
        $this->registrationType = self::REGISTRATION_TYPES[array_rand(self::REGISTRATION_TYPES)];
        $this->birthDate = $this->generateBirthDate();
        $this->registrationDate = $this->generateRegistrationDate();
        $this->moveOutDate = $this->generateMoveOutDate();
    }

    private function generateSnils(): string
    {
        $firstRatio = [4, 8, 7, 4, 11, 9, 5, 3, 8];
        $secondRatio = [2, 4, 8, 7, 4, 11, 9, 5, 3, 8];
        $k1 = 0;
        $k2 = 0;
        $nineRandom = mt_rand(100000000, 999999999);
        foreach (array_map('intval', str_split($nineRandom)) as $key => $item) {
            $k1 += $firstRatio[$key] * $item;
            $k2 += $secondRatio[$key] * $item;
        }

        $k1 = 11 - $k1 % 11;
        if ($k1 >= 10) {
            $k1 = 0;
        }
        $k2 = 11 - ($k2 + $k1 * $secondRatio[9]) % 11;
        if ($k2 >= 10) {
            $k2 = 0;
        }
        return $nineRandom . $k1 . $k2;

    }

    private function generateBirthDate(): string
    {
        $randomTimestamp = mt_rand(strtotime('01.01.1940'), strtotime('01.01.2005'));
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate->format("Y-m-d");
    }

    private function generateRegistrationDate(): string
    {
        $futureDate = strtotime('+14 year', strtotime($this->birthDate));
        $randomTimestamp = mt_rand($futureDate, time());
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate->format("Y-m-d");
    }

    private function generateMoveOutDate(): null|string
    {
        $futureDate = strtotime('+5 year', time());
        $randomTimestamp = mt_rand(strtotime($this->registrationDate), $futureDate);
        if ($randomTimestamp > time()) {
            return null;
        }
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate->format("Y-m-d");
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getSecondName(): string
    {
        return $this->secondName;
    }

    public function getSnils(): string
    {
        return $this->snils;
    }

    public function getRegistrationType(): string
    {
        return $this->registrationType;
    }

    public function getBirthDate(): string
    {
        return $this->birthDate;
    }

    public function getRegistrationDate(): string
    {
        return $this->registrationDate;
    }

    public function getMoveOutDate(): ?string
    {
        return $this->moveOutDate;
    }
}