<?php

namespace repository;

use database\Database;
use PDOStatement;
use Person;

include_once __DIR__ . '/../database/Database.php';
include_once __DIR__ . "/../entity/Person.php";

class PersonRepository
{
    private Database $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getAllPeople(): PDOStatement
    {
        $connection = $this->database->createConnection();
        $sql = "SELECT * FROM Person";
        $result = $connection->query($sql);
        $this->database->closeConnection($connection);
        return $result;
    }

    public function getPeopleFilteredByName($value): PDOStatement
    {
        $value = "%" . $value . "%";
        $connection = $this->database->createConnection();
        $sql = "SELECT * FROM Person WHERE concat(first_name, ' ', surname, ' ', second_name) LIKE :searchValue";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(':searchValue', $value);
        $stmt->execute();
        $this->database->closeConnection($connection);
        return $stmt;
    }

    public function initDatabase(): void
    {
        $this->createTable();
        if (!$this->isTableFilled()) {
            $this->fillTableWithRandomPerson();
        }
    }

    private function createTable(): void
    {
        $connection = $this->database->createConnection();
        $sql = "CREATE TABLE IF NOT EXISTS Person(
            id serial PRIMARY KEY, 
            first_name varchar(20) not null, 
            surname varchar(20) not null, 
            second_name varchar(20) not null,
            snils varchar(11) UNIQUE not null, 
            registration_type varchar(15) not null, 
            birth_date date not null,
            registration_date date not null, 
            move_out_date date);";
        $connection->exec($sql);
        $this->database->closeConnection($connection);
    }

    private function fillTableWithRandomPerson(): void
    {
        $connection = $this->database->createConnection();
        $sql = "INSERT INTO Person (first_name, surname, second_name, snils, registration_type, 
                    birth_date, registration_date, move_out_date) 
                    VALUES (:first_name, :surname, :second_name, :snils, :registration_type, 
                    :birth_date, :registration_date, :move_out_date)";
        for ($i = 0; $i < 10; $i++) {
            $person = new Person();
            $stmt = $connection->prepare($sql);
            $stmt->bindValue(":first_name", $person->getFirstName());
            $stmt->bindValue(":surname", $person->getSurname());
            $stmt->bindValue(":second_name", $person->getSecondName());
            $stmt->bindValue(":snils", $person->getSnils());
            $stmt->bindValue(":registration_type", $person->getRegistrationType());
            $stmt->bindValue(":birth_date", $person->getBirthDate());
            $stmt->bindValue(":registration_date", $person->getRegistrationDate());
            $stmt->bindValue(":move_out_date", $person->getMoveOutDate());
            $stmt->execute();
        }
        $this->database->closeConnection($connection);
    }

    private function isTableFilled(): bool
    {
        $connection = $this->database->createConnection();
        $sql = "SELECT id from Person";
        $result = $connection->query($sql)->rowCount() > 0;
        $this->database->closeConnection($connection);
        return $result;
    }
}