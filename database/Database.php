<?php

namespace database;

use PDO;
use PDOException;

class Database
{
    private string $connectionString = "mysql:host=localhost;port=3333;dbname=mysqldb";
    private string $username = "user";
    private string $password = "password";

    public function createConnection()
    {
        try {
           return new PDO($this->connectionString, $this->username, $this->password);
        }
        catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function closeConnection(PDO &$connection): void
    {
        $connection = null;
    }
}