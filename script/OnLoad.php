<?php

use repository\PersonRepository;

include_once __DIR__ . '/../repository/PersonRepository.php';
include_once __DIR__ . '/../script/FillTable.php';

$personRepository = new PersonRepository();
$personRepository->initDatabase();

FillTable::fillTable($personRepository->getAllPeople());