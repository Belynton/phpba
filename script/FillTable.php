<?php

class FillTable
{
    public static function fillTable(PDOStatement $statement): void
    {
        foreach ($statement as $row) {
            echo "<tr>";
            echo "<td>" . $row["surname"] . "</td>";
            echo "<td>" . $row["first_name"] . "</td>";
            echo "<td>" . $row["second_name"] . "</td>";
            echo "<td>" . $row["snils"] . "</td>";
            echo "<td>" . $row["registration_type"] . "</td>";
            echo "<td>" . $row["birth_date"] . "</td>";
            echo "<td>" . $row["registration_date"] . "</td>";
            echo "<td>" . $row["move_out_date"] . "</td>";
            echo "<td>" . self::calculate_age($row["birth_date"]) . "</td>";
            echo "</tr>";
        }
    }

    private static function calculate_age($birthday): int
    {
        $birthday_timestamp = strtotime($birthday);
        $age = date('Y') - date('Y', $birthday_timestamp);
        if (date('md', $birthday_timestamp) > date('md')) {
            $age--;
        }
        return $age;
    }
}