<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BA Task</title>
    <link rel="stylesheet" href="style/style.css">
</head>
<body>
<div class="user-zone">
    <div class="command-elements">
        <form action="" METHOD="GET">
            <input type="text" name="search">
            <input type="submit" value="Поиск">
        </form>
    </div>
    <div class="result-table" id="loading-area">
        <table>
            <tr>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>СНИЛС</th>
                <th>Тип регистрации</th>
                <th>Дата рождения</th>
                <th>Дата прописки</th>
                <th>Дата выписки</th>
                <th>Возраст</th>
            </tr>
            <?php
            if (empty($_GET['search'])) {
                require_once 'script/OnLoad.php';
            } else {
                require_once 'script/SearchByName.php';
            }
            ?>
        </table>
    </div>
</div>
</body>
</html>